---
title: "Accesso Civico Generalizzato (FOIA)"
date: 2017-09-09T14:56:41+02:00
draft: false
type: "foia"
author: "Confiscati Bene team"
---

Con questa semplice pagina **puoi creare facilmente la richiesta dell'elenco dei beni confiscati del comune di tuo interesse**. <br> Lo farai sfruttando il tuo diritto di accesso civico generalizzato di cui al D.Lgs 33/2013 e che ti permette di chiedere autonomamente dati, documenti e informazioni alle pubbliche amministrazioni italiane.<br>
**Sono 824 i Comuni destinatari di beni confiscati secondo i dati Open Regio (aggiornati al gg/mm/aaaa). Sono loro i nostri destinatari...anzi...i tuoi :)

## Cosa chiedi (è tutto precompilato...non preoccuparti 😉)

(1) l’elenco aggiornato dei beni confiscati contenente i dati di cui all’art. 48 comma 3 lett. c) del Dlgs. 159/2011 cioè “*la consistenza,la destinazione e l’utilizzazione dei beni nonché, in caso di assegnazione a terzi, i dati identificativi del concessionario e gli estremi, l’oggetto e la durata dell’atto di concessione*” <br>
(2) la contestuale pubblicazione di tale elenco aggiornato nella sezione *Amministrazione Trasparente/Beni immobili e gestione patrimonio/Patrimonio immobiliare* ritenendo tale adempimento un obbligo ai sensi dell’art. 30 del D. Lgs. 33/2013

## Con quale strumento?

Confiscati bene si serve di <a target="_blank" href="http://www.foiapop.it/"><strong>FoiaPop</strong></a>, una piattaforma online per la compilazione di richieste di accesso civico semplice e generalizzato, nata per facilitare la creazione della richiesta.

## Come funziona
Seleziona il comune per il quale vuoi creare la richiesta, clicca quindi su "*Crea richiesta*". Verrai reindirizzato al modulo online da compilare soltanto con i tuoi dati. Troverai precompilati sia il testo della richiesta sia il destinatario. Andando quindi "*Avanti*"  potrai visionare una anteprima.
<br>Infine "*Crea Richiesta Foia*" ed ecco il tuo pdf da scaricare.

## Il successivo invio al Comune

Potrai inviare la richiesta al Comune in alternativa tramite:
<br>*Trasmissione telematica*: tramite e-mail o PEC all'indirizzo di riferimento della pubblica amministrazione (sottoscrivendo il modulo e allegando una copia del documento di riconoscimento valido oppure apponendo la firma digitale, in questo caso non è necessario allegare copia del documento di riconoscimento);
<br>*Trasmissione analogica*: tramite posta o raccomandata o brevi manu al protocollo della pubblica amministrazione (sottoscrivendo il modulo e allegando una copia del documento di riconoscimento valido).

## Attenzione! E puoi fare ancora di più 💪

- se invii una richiesta per il tuo comune - una volta fatto - puoi per favore <a target="_blank" href="#"><strong>indicarlo qui</strong></a>? In questo modo altri utenti sapranno se per un dato comune è già stata fatta una richiesta (<a href="#" target="_blank">qui l'elenco attuale</a>);
- quando riceverai i dati o il link all'elenco dei beni confiscati, <strong>inviaceli per favore per email</strong> a <a href="mailto:info@confiscatibene.it?subject=[Italia a Fuoco] Allego elenco beni confiscati">info@confiscatibene.it</a>. Così creeremo un catalogo unico e lo renderemo disponibile nel sito;
- Aiutaci a promuovere la campagna sui social usando l'hashtag #confiscatiFOIA!

Grazie!